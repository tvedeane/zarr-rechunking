import zarr
import numcodecs

import sys
import shutil
import json
import os


def get_absolute_path(path):
    return os.path.join(os.getcwd(), path)


def get_zarr_size(absolute_path):
    items = os.listdir(absolute_path)
    return len(items) - 1


def set_shape_size_to_1(absolute_path):
    filename = os.path.join(absolute_path, '.zarray')
    with open(filename) as file:
        data = json.load(file)

    data['shape'] = [1, 1]

    with open(filename, 'w') as file:
        json.dump(data, file, indent=2)


def copy_zarray_file(input_dir, intermediate_dir):
    os.mkdir(intermediate_dir)
    zarray_filename = '.zarray'
    shutil.copy(os.path.join(input_dir, zarray_filename), os.path.join(intermediate_dir, zarray_filename))


def copy_nth_chunk(input_dir, chunk_number, intermediate_dir):
    input_chunk_path = os.path.join(input_dir, str(chunk_number) + '.0')
    output_chunk_path = os.path.join(intermediate_dir, '0.0')
    shutil.copy(input_chunk_path, output_chunk_path)


def remove_temp_dir(intermediate_dir):
    shutil.rmtree(intermediate_dir)


if len(sys.argv) != 2:
    print("Usage: python solution.py <zarr_dataset_directory>")
else:
    input_dir = get_absolute_path(sys.argv[1])

    size = get_zarr_size(input_dir)
    print('Size is', size)

    temp_dir = get_absolute_path('data/temp.zarr')
    copy_zarray_file(input_dir, temp_dir)
    set_shape_size_to_1(temp_dir)

    output_dir = 'data/solution.zarr'
    z2 = zarr.open(output_dir, mode='w', shape=(size, 1), chunks=(size, None), dtype='i4')

    for i in range(size):
        copy_nth_chunk(input_dir, i, temp_dir)
        z1 = zarr.open(temp_dir)  # reads one chunk of the input data
        z2[i, 0] = z1[0, 0]

    remove_temp_dir(temp_dir)
    print('Please find the output in', output_dir)
