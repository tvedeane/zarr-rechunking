# Problem description

You are given a Zarr dataset with a one-dimensional variable that has N data points and one data point per chunk. Implement a program that rechunks this variable to contain N data points per chunk, i.e. merge all data points into one chunk. This rechunking must be implemented in-place, i.e. you cannot read the dataset and then produce a new dataset with a rechunked variable.

## How I understand the problem to solve:

* it has to be implemented in-place, i.e. it modifies the input in place, without creating a separate copy of the data structure
* geospatial data is usually chunked by day, but it is common to analyse the data for a given point over several days, so re-chunking from day to location is a commonly used functionality

## Installation

```commandline
pip3 install numpy
pip3 install zarr
pip3 install numcodecs
```

## Zarr-related examples in Python

```python
import zarr
z1 = zarr.zeros((10, 1), chunks=(1, None), dtype='i4')
```

This creates a 1-dimensional array of 32-bit integers, divided into chunks, where each chunk is 1 integer (so there will be 10 chunks). This could be used as an example input. And the algorithm would have to convert it into something like this:

```python
z2 = zarr.zeros((10, 1), chunks=(10, None), dtype='i4')
```

This is an array of length 10, where all integers are in the same chunk.

# Algorithm

1. Calculate size of the input data (can be achieved by counting the files with numbers e.g. 0.0, 1.0).
2. Copy `.zarray` file from input dataset to a temporary directory.
3. Modify the temporary `.zarray` by changing the shape JSON property to `[1,1]` array 
(so at maximum only one item will be read).
4. Create new empty zarr dataset (output) with 1 chunk of size equal to the value from 1.
5. Start a loop with iterations count equal to size taken from 1.
6. Inside the loop:
* copy n-th chunk (where `n` is the iteration counter) to temporary folder and save it as `0.0`
* read zarr dataset from temporary directory (size of 1 chunk)
* append it to the output dataset

## Memory considerations

This algorithm avoids storing the input dataset in memory twice.
Memory usage increases linearly: each append requires allocation of extra bytes.
In total algorithm will require `n*k + k` amount of memory, 
where `n` is count of chunks in the input data and `k` is the size of a chunk.

## Running

### Example data preparation

If you don't have any Zarr dataset available you can generate example dataset of length 20 like that:

```commandline
python3.11 prepare_data.py 20
```

### Starting rechunking process

To start the rechunking on dataset found in `data/incr.zarr` call:

```commandline
python3.11 solution.py data/incr.zarr
```

### Debugging

Use this to display basic information about any Zarr dataset:

```commandline
python3.11 display_zarr_dataset.py data/incr.zarr
```

## Alternative solution in Rust

This is more experimental, because it doesn't depend on any Zarr library (but it depends on `blocs` - a compression library). Its functionality is probably limited, because will not work for Zarr datasets based on different compression algorithm than blosc.

### Setup

I had to execute this before performing `cargo build` (or `cargo run`):

```
export LIBRARY_PATH=$LIBRARY_PATH:$(brew --prefix)/lib:$(brew --prefix)/opt/c-blosc/lib
```

### Starting

Can be started from `rust` directory using this command:

```commandline
cargo run data/incr.zarr
```
