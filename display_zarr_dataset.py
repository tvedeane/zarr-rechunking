import zarr
import sys


if len(sys.argv) != 2:
    print("Usage: python solution.py <zarr_dataset_directory>")
else:
    input_dir = sys.argv[1]
    z = zarr.open(input_dir)
    print(z[:])
    print("Chunks are:", z.chunks)

