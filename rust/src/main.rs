use std::env;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io;

use blosc::decompress_bytes;
use blosc::Clevel;

use serde_json::Value;

use tempfile::tempdir;
use assert_json_diff::assert_json_eq;

fn get_zarr_size(folder_path: &str) -> Result<usize, std::io::Error> {
    let entries = fs::read_dir(folder_path)?;

    let mut file_count = 0;

    for entry in entries {
        if let Ok(entry) = entry {
            let path = entry.path();
            if path.is_file() {
                file_count += 1;
            }
        }
    }

    Ok(file_count - 1)
}

fn create_zarray_file(source_path: &String, destination_path: &String, zarr_size: usize) -> io::Result<()> {
    let mut file = File::open(source_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let mut json_data: Value = serde_json::from_str(&contents)?;

    if let Some(obj) = json_data.as_object_mut() {
        if let Some(property) = obj.get_mut("chunks") {
            if let Some(arr) = property.as_array_mut() {
                *arr = vec![Value::Number(zarr_size.into()), Value::Number(1.into())];
            }
        }
    }

    let modified_contents = serde_json::to_string_pretty(&json_data)?;

    let _ = fs::write(destination_path, modified_contents);

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Usage: cargo run <zarr_dataset_directory>");
        return;
    }

    let dataset_location = &args[1];
    let zarr_size = get_zarr_size(dataset_location).unwrap();

    let mut output_data = Vec::new();

    for i in 0..zarr_size {
        let file_path = format!("{}/{i}.0", dataset_location);

        let mut file = match File::open(file_path) {
            Ok(file) => file,
            Err(err) => {
                eprintln!("Error opening the file: {}", err);
                return;
            }
        };

        let file_size = match file.metadata() {
            Ok(metadata) => metadata.len(),
            Err(err) => {
                eprintln!("Error reading file metadata: {}", err);
                return;
            }
        };

        let mut compressed_data = vec![0; file_size as usize];

        match file.read_exact(&mut compressed_data) {
            Ok(_) => {
                unsafe {
                    let decompressed_data: Vec<u32> = match decompress_bytes(&compressed_data) {
                        Ok(data) => data,
                        Err(err) => {
                            eprintln!("Error decompressing the file: {}", err);
                            return;
                        }
                    };

                    output_data.insert(i, decompressed_data[0]);
                }
            }
            Err(err) => {
                eprintln!("Error reading the file: {}", err);
                return;
            }
        }
    }

    let ctx = blosc::Context::new().clevel(Clevel::L5);
    let compressed_output = ctx.compress(&output_data);

    let output_dir = "data/output.zarr";
    let _ = fs::create_dir(output_dir);

    let output_path = format!("{}{}", output_dir, "/0.0");
    let _ = fs::write(output_path, compressed_output);

    let input_zarray = format!("{}{}", dataset_location, "/.zarray");
    let output_zarray = format!("{}{}", output_dir, "/.zarray");
    let _ = create_zarray_file(&input_zarray, &output_zarray, zarr_size);
        
    println!("Rechunked {} chunks and written to `{}`", zarr_size, output_dir);
}

#[test]
fn test_create_zarray_file() {
    let temp_dir = tempdir().expect("Failed to create temporary directory");
    let source_path = temp_dir.path().join("source.txt").into_os_string().into_string().unwrap();
    let destination_path = temp_dir.path().join("destination.txt").into_os_string().into_string().unwrap();

    let data = "{\"chunks\": [1, 2], \"prop\": \"value\"}";
    let _ = fs::File::create(&source_path).expect("Failed to create source file");
    let _ = fs::write(&source_path, data);

    let _ = create_zarray_file(&source_path, &destination_path, 12);

    let mut destination_file =
        fs::File::open(&destination_path).expect("Failed to open destination file");
    let mut contents = String::new();
    let _ = destination_file.read_to_string(&mut contents);
    let actual_data: Value = serde_json::from_str(&contents).unwrap();

    let expected_data: Value = serde_json::from_str("{\"chunks\": [12, 1], \"prop\": \"value\"}").unwrap();
    assert_json_eq!(actual_data, expected_data);
}
