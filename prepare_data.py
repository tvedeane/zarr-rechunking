import zarr
import numcodecs

import sys

if len(sys.argv) != 2:
    print("Usage: python solution.py <shape_length>")
    print("Running with default length 10.")
    shape_len = 10
else:
    shape_len = sys.argv[1]

z1 = zarr.open('data/incr.zarr', mode='w', shape=(shape_len, 1), chunks=(1, None), dtype='i4')

for x in range(int(shape_len)):
    z1[x, 0] = x+100

